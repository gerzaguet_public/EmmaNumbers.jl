using Flux 

x = 6
m = Chain(
        Chain([
        Conv((7,), 2 => 64, pad=3, stride=2, bias=false),  # 9_408 parameters
        BatchNorm(64, relu),              # 128 parameters, plus 128
        MaxPool((3,), pad=1, stride=2),
        Parallel(
            Chain(
            Conv((3,), 64 => 64, pad=1, bias=false),  # 36_864 parameters
            BatchNorm(64, relu),          # 128 parameters, plus 128
            Conv((3,), 64 => 64, pad=1, bias=false),  # 36_864 parameters
            BatchNorm(64),                # 128 parameters, plus 128
            ),
            identity,
        ),
        Parallel(
            Chain(
            Conv((3,), 64 => 64, pad=1, bias=false),  # 36_864 parameters
            BatchNorm(64, relu),          # 128 parameters, plus 128
            Conv((3,), 64 => 64, pad=1, bias=false),  # 36_864 parameters
            BatchNorm(64),                # 128 parameters, plus 128
            ),
            identity,
        ),
        Parallel(
            Chain(
            Conv((3,), 64 => 128, pad=1, stride=2, bias=false),  # 73_728 parameters
            BatchNorm(128, relu),         # 256 parameters, plus 256
            Conv((3,), 128 => 128, pad=1, bias=false),  # 147_456 parameters
            BatchNorm(128),               # 256 parameters, plus 256
            ),
            Chain([
            Conv((1, ), 64 => 128, stride=2, bias=false),  # 8_192 parameters
            BatchNorm(128),               # 256 parameters, plus 256
            ]),
        ),
        Parallel(
            Chain(
            Conv((3,), 128 => 128, pad=1, bias=false),  # 147_456 parameters
            BatchNorm(128, relu),         # 256 parameters, plus 256
            Conv((3,), 128 => 128, pad=1, bias=false),  # 147_456 parameters
            BatchNorm(128),               # 256 parameters, plus 256
            ),
            identity,
        ),
        Parallel(
            Chain(
            Conv((3,), 128 => 256, pad=1, stride=2, bias=false),  # 294_912 parameters
            BatchNorm(256, relu),         # 512 parameters, plus 512
            Conv((3,), 256 => 256, pad=1, bias=false),  # 589_824 parameters
            BatchNorm(256),               # 512 parameters, plus 512
            ),
            Chain([
            Conv((1,), 128 => 256, stride=2, bias=false),  # 32_768 parameters
            BatchNorm(256),               # 512 parameters, plus 512
            ]),
        ),
        Parallel(
            Chain(
            Conv((3,), 256 => 256, pad=1, bias=false),  # 589_824 parameters
            BatchNorm(256, relu),         # 512 parameters, plus 512
            Conv((3,), 256 => 256, pad=1, bias=false),  # 589_824 parameters
            BatchNorm(256),               # 512 parameters, plus 512
            ),
            identity,
        ),
        Parallel(
            Chain(
            Conv((3,), 256 => 512, pad=1, stride=2, bias=false),  # 1_179_648 parameters
            BatchNorm(512, relu),         # 1_024 parameters, plus 1_024
            Conv((3,), 512 => 512, pad=1, bias=false),  # 2_359_296 parameters
            BatchNorm(512),               # 1_024 parameters, plus 1_024
            ),
            Chain([
            Conv((1, ), 256 => 512, stride=2, bias=false),  # 131_072 parameters
            BatchNorm(512),               # 1_024 parameters, plus 1_024
            ]),
        ),
        Parallel(
            Chain(
            Conv((3,), 512 => 512, pad=1, bias=false),  # 2_359_296 parameters
            BatchNorm(512, relu),         # 1_024 parameters, plus 1_024
            Conv((3,), 512 => 512, pad=1, bias=false),  # 2_359_296 parameters
            BatchNorm(512),               # 1_024 parameters, plus 1_024
            ),
            identity,
        ),
        ]),
        Chain(
            AdaptiveMeanPool((1,)),
            Flux.flatten,
            Dense(512 => x),               # 513_000 parameters
            Flux.softmax
        ),
    )