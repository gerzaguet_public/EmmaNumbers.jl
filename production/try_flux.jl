using EmmaNumbers 
using Flux 


function mse(x::AbstractMatrix{T},x̄::AbstractMatrix{R}) where {T,R<:EmmaNumber}
    mse = 10*log10.(sum( abs2.(x[:] .- T.(x̄[:]))) / sum(abs2.(x[:])) )
end
function mse(x::AbstractMatrix{T},x̄::AbstractMatrix{R}) where {T,R}
    mse = 10*log10.(sum( abs2.(x[:] .- x̄[:])) / sum(abs2.(x[:])) )
end

# ----------------------------------------------------
# --- Networks 
# ----------------------------------------------------
dr = 0.5 

lenet =  Chain(
    Conv((3, 3), 1=>16, relu),
    MaxPool((2, 2)),
    Conv((3, 3), 16=>32, relu),
    MaxPool((2, 2)),
    Flux.flatten,
    Dense(6272, 17),
    Dropout(0.5), 
    softmax,
   )



vgg = Flux.Chain(
    Conv((3, 3), 1 => 32, relu),
    BatchNorm(32),
    Conv((3, 3), 32 => 32, relu),
    BatchNorm(32),
    MaxPool((2, 2)),
    Dropout(0.2), Conv((3, 3), 32 => 64, relu),
    BatchNorm(64),
    Conv((3, 3), 64 => 64, relu),
    BatchNorm(64),
    MaxPool((2, 2)),
    Dropout(0.2), Conv((3, 3), 64 => 128, relu),
    BatchNorm(128),
    Conv((3, 3), 128 => 128, relu),
    BatchNorm(128),
    MaxPool((2, 2)),
    Dropout(0.2), GlobalMeanPool(),
    Flux.flatten,
    Dense(128 => 512),
    Dropout(0.2),
    Dense(512 => 17),
    softmax,
   )

minidense = Chain(
    Flux.flatten,
    Dense(64*64 => 144, relu),
    Dense(144 => 6, relu),
)

# chain = Chain(
#     Flux.flatten,
#     Dense(64 => 32, relu),
#     BatchNorm(32),
#     Dense(32 => 6, relu),
# )


chain = Flux.Chain(
    Conv((5, 5), 1 => 16, relu),
    Conv((5, 5), 16 => 16, relu),
    Conv((3, 3), 16 => 16, relu),
    MaxPool((2, 2)),
    Dropout(0.2),
    Conv((3, 3), 16 => 32, relu),
    Conv((3, 3), 32 => 32, relu),
    Conv((1, 1), 32 => 32, relu),
    GlobalMaxPool(),
    Dropout(0.2),
    Flux.flatten,
    Dense(32 => 256, relu),
    Dropout(0.5),
    Dense(256 => 17),
    softmax,
   )


dnn = Chain(
        Flux.flatten,
        Dense(256*2, 100),
        Dropout(dr),
        leakyrelu,
        Dense(100, 64),
        Dropout(dr),
        leakyrelu,
        Dense(64,6),
        Flux.softmax
        )


arroyo = Chain(
    Conv((10,), 2 => 64, pad=SamePad(), relu),
    MaxPool((2,)),
    Conv((10,), 64 => 32, pad=SamePad(), relu),
    MaxPool((2,)),
    Conv((10,), 32 => 16, pad=SamePad(), relu),
    MaxPool((2,)),
    Flux.flatten,
    Dense(512,64),
    Dense(64,4),
    Dense(4,6),
    Flux.softmax
        )


dr = 0.5
elmaghbub = Chain(
    #x -> reshape(x, (size(x)[1], 2, 1, size(x)[3])),
    Conv((4,), 2 => 32, pad=SamePad()),
    BatchNorm(32),
    leakyrelu,
    MaxPool((2,), stride=2),
    Conv((4,), 32 => 48, pad=SamePad()),
    BatchNorm(48),
    leakyrelu,
    MaxPool((2,), stride=2),
    Conv((4,), 48 => 64, pad=SamePad()),
    BatchNorm(64),
    leakyrelu,
    MaxPool((2,), stride=2),
    Conv((4,), 64 => 76, pad=SamePad()),
    BatchNorm(76),
    leakyrelu,
    MaxPool((2,), stride=2),
    Conv((4,), 76 => 96, pad=SamePad()),
    BatchNorm(96),
    leakyrelu,
    MaxPool((2,), stride=2),
    Conv((4,), 96 => 110, pad=SamePad()),
    BatchNorm(110),
    leakyrelu,
    MaxPool((2,), stride=2),
    Flux.flatten,
    Dense(440, 100),
    Dropout(dr),
    leakyrelu,
    Dense(100, 64),
    Dropout(dr),
    leakyrelu,
    Dense(64,6),
    Flux.softmax
)

CNN_GRU = Chain(
            Conv((1,), 2 => 32),
            BatchNorm(32),
            relu,
            Conv((3,), 32 => 32, pad=SamePad(), relu),
            BatchNorm(32),
            relu,
            MaxPool((2,)),
            Conv((3,), 32 => 32, pad=SamePad(), relu),
            BatchNorm(32),
            relu,
            MaxPool((2,)),
            Conv((3,), 32 => 32, pad=SamePad(), relu),
            BatchNorm(32),
            relu,
            MaxPool((2,)),
            x -> permutedims(x, (2, 1, 3)),
            Flux.Recur(Flux.GRUCell(32, 50)),
            x-> x[:,end,:],
            Dense(50, 6),
            Flux.softmax
        )


# ----------------------------------------------------
# --- Data 
# ----------------------------------------------------

networks = [lenet, vgg, dnn, arroyo, elmaghbub]


networks = [dnn, arroyo, elmaghbub]
sig = randn(Float32,256,2,1)
sigEmma    = EmmaNumber.(sig)


# for (cnt,n) ∈ enumerate(networks) 
#     @info "Computing Flops for network"
#     display(n)
#     # Reference 
#     out = n(sig)
#     # Flops 
#     chainEmma = convert_network_emma(n)
#     outEmma = @count_emma chainEmma(sigEmma)
#     # MSE 
#     ϵ = round(mse(out,outEmma);digits=2) 
#     @info "MSE for Flops is $ϵ dB"
# end

n = dnn 
out = n(sig)
# Flops 
chainEmma = convert_network_emma(n)
outEmma = @count_emma chainEmma(sigEmma)
tab = copy(EmmaNumbers.FLOPS)
@show ϵ = mse(outEmma,out)


n = arroyo 
out = n(sig)
# Flops 
chainEmma = convert_network_emma(n)
outEmma = @count_emma chainEmma(sigEmma)
tab2= EmmaNumbers.FLOPS.mul
@show ϵ = mse(outEmma,out)


n = CNN_GRU 
out = n(sig)
# Flops 
chainEmma = convert_network_emma(n)
outEmma = @count_emma chainEmma(sigEmma)
tab2= EmmaNumbers.FLOPS.mul
@show ϵ = mse(outEmma,out)
