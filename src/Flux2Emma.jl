
using Flux 





""" Replace layer index `index_layer` by a new layer `new_layer`
"""
function change_layer(model,index_layer,new_layer)
    return Chain(model.layers[1:index_layer-1]..., new_layer, model.layers[index_layer+1:end]...)
end



""" Remove the `index_layer` layer of the model 
"""
function remove_layer_idx(model,index_layer)
    toKeep = filter(x->!(x in index_layer), 1:length(model))
    model2 = Chain([model.layers[toKeep]...])
end



""" Remove Dropout as there is an hardcoded float() operator inside 
"""
function remove_layer(model,layerToRemove)
    index_rm = [] 
    for (idx,layer) in enumerate(model)
        if isa(layer,layerToRemove)
            push!(index_rm,idx)
            #push!(index_dropout,idx)
        end
    end
    model2 = remove_layer_idx(model,index_rm)
    return model2
end

function convert_layer_rec(layer)
    if layer isa Flux.RNNCell
        s = EmmaNumber.(layer.σ) 
        wi = EmmaNumber.(layer.Wi)
        wh = EmmaNumber.(layer.Wh)
        b = EmmaNumber.(layer.b)
        g = Flux.RNNCell(s,wi,wh,b,layer.state0)    
    elseif layer isa Flux.LSTMCell 
        wi = EmmaNumber.(layer.Wi)
        wh = EmmaNumber.(layer.Wh)
        b = EmmaNumber.(layer.b)
        g = Flux.LSTMCell(wi,wh,b,layer.state0)   
    elseif layer isa Flux.GRUCell 
        wi = EmmaNumber.(layer.Wi)
        wh = EmmaNumber.(layer.Wh)
        b = EmmaNumber.(layer.b)
        g = Flux.GRUCell(wi,wh,b,layer.state0)  
    elseif layer isa Flux.GRUv3Cell 
        wi = EmmaNumber.(layerl.Wi)
        wh = EmmaNumber.(layer.Wh)
        whh= EmmaNumber.(layer.Wh_h̃)
        b = EmmaNumber.(layer.b)
        g = Flux.GRUv3Cell(wi,wh,b,whh,layer.state0)  
    else 
        g = layer
    end
    return g
end


function convert_layer_emma(model,il)
    l  = model[il] 
    if l isa Dense 
        w = EmmaNumber.(l.weight)
        b = EmmaNumber.(l.bias)
        d = Dense(w,b,l.σ);
        model2 = change_layer(model,il,d)
    elseif l isa Conv 
        w = EmmaNumber.(l.weight)
        b = EmmaNumber.(l.bias)
        c = Conv(w,b,l.σ;l.stride,l.pad,l.dilation)
        model2 = change_layer(model,il,c)
    elseif l isa Flux.Recur
        cell = l.cell
        c = convert_layer_rec(cell)
        r = Flux.Recur(c, l.state)
        model2 = change_layer(model, il, r)
    else 
        model2 = model 
    end
    return model2
end

function convert_network_emma(model)
    model2 = model
    for (il,l) in enumerate(model)
        # Switch to Emma numbering  
        model2 = convert_layer_emma(model2,il)
    end 
    model3 = remove_layer(model2,Dropout)
    return model3
end

